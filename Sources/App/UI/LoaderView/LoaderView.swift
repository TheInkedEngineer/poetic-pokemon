//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit

class LoaderView: UIView {
  
  // MARK: - UI Elements
  
  let spinner = UIActivityIndicatorView()
  
  // MARK: - Init
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setup()
    style()
    layout()
  }
  
  required init?(coder: NSCoder) {
    nil
  }
  
  // MARK: - Setup, Style, Update, Layout
  
  func setup() {
    addSubview(spinner)
    
    spinner.startAnimating()
  }
  
  func style() {
    Self.styleView(self)
    Self.styleSpinner(spinner)
  }
  
  func update() {}
  
  func layout() {
    spinner.translatesAutoresizingMaskIntoConstraints = false
    
    [
      spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
      spinner.centerYAnchor.constraint(equalTo: centerYAnchor),
      spinner.heightAnchor.constraint(equalToConstant: 50),
      spinner.widthAnchor.constraint(equalToConstant: 50)
    ].forEach {
      $0.isActive = true
    }
  }
}

// MARK: - Styling

private extension LoaderView {
  static func styleView(_ view: UIView) {
    view.backgroundColor = .tertiarySystemBackground.withAlphaComponent(0.7)
  }
  
  static func styleSpinner(_ spinner: UIActivityIndicatorView) {
    spinner.style = .medium
  }
}
