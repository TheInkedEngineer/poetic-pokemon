//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit

class BaseViewController: UIViewController {
  /// The dependencies of the application.
  let dependencies: Dependencies
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    nil
  }
}
