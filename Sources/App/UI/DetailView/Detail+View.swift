//
//  Poetic Pokemon
//  Firas Safa
//

import Combine
import PoeticPokemonSDK
import SwiftUI

struct DetailView: View {
  let detailViewModel: DetailViewModel
  
  var body: some View {
    VStack {
      PoeticPokemon.UI.PokeSprite(url: detailViewModel.spriteURL)
        .padding()
      
      Text(detailViewModel.pokemonName)
        .font(.title)
        .bold()
        .shadow(radius: 20)
        .padding()
      
      PoeticPokemon.UI.PoemCard(text: detailViewModel.description)
        .padding()
    }
  }
}
