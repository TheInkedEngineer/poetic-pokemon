//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

struct DetailViewModel {
  let pokemonName: String
  let spriteURL: URL
  let description: String
}
