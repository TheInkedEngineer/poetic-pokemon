//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit
import SwiftUI

class DetailViewController: UIViewController {
  private let detailViewModel: DetailViewModel
  
  init(detailViewModel: DetailViewModel) {
    self.detailViewModel = detailViewModel
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    nil
  }
  
  override func viewDidLoad() {
    let swiftUIView = DetailView(detailViewModel: detailViewModel)
    let hostingController = UIHostingController(rootView: swiftUIView)
    present(hostingController, animated: true, completion: nil)
  }
}
