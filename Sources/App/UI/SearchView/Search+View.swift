//
//  Poetic Pokemon
//  Firas Safa
//

import Combine
import UIKit
import Utils

class SearchView: UIView {
  var viewModel: SearchViewModel
  private var bindings = Set<AnyCancellable>()
  
  // MARK: UI Elements
  
  let title = UILabel()
  let explanation = UILabel()
  let searchBar = UITextField()
  let actionButton = UIButton()
  
  // MARK: Init
  
  init(viewModel: SearchViewModel) {
    self.viewModel = viewModel
    
    super.init(frame: .zero)
    
    setup()
    style()
    bind()
    layout()
  }
  
  required init?(coder: NSCoder) {
    nil
  }
  
  // MARK: Setup, Style, Update, Layout
  
  func setup() {
    addSubview(title)
    addSubview(explanation)
    addSubview(searchBar)
    addSubview(actionButton)
    
    actionButton.addTarget(self, action: #selector(tappedActionButton), for: .touchUpInside)
  }
  
  func style() {
    Self.styleView(self)
    Self.styleTitle(title)
    Self.styleSubtitle(explanation)
    Self.styleTextField(searchBar)
    Self.styleButton(actionButton)
  }
  
  func bind() {
    searchBar.textPublisher
      .assign(to: \.pokemonName, on: viewModel)
      .store(in: &bindings)
    
    viewModel.$pokemonName
      .receive(on: RunLoop.main)
      .map {
        $0.count >= 3 // Mew is the shortest Pokemon name with 3 characters
      }
      .assign(to: \.isEnabled, on: actionButton)
      .store(in: &bindings)
    
    
    viewModel.$error.sink { [weak self] error in
      guard let err = error else {
        return
      }
      
      self?.viewModel.showErrorAlert(for: err)
    }
    .store(in: &bindings)
  }
  
  func layout() {
    let sidePadding: CGFloat = 16
    let elementSpacing: CGFloat = 48
    
    [title, explanation, searchBar, actionButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
    
    [
      searchBar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -elementSpacing),
      searchBar.heightAnchor.constraint(equalToConstant: 40),
      searchBar.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: sidePadding),
      searchBar.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -sidePadding),
      
      actionButton.centerXAnchor.constraint(equalTo: searchBar.centerXAnchor),
      actionButton.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: elementSpacing),
      actionButton.heightAnchor.constraint(equalToConstant: 40),
      actionButton.widthAnchor.constraint(equalToConstant: 120),
      
      explanation.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
      explanation.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor),
      explanation.bottomAnchor.constraint(equalTo: searchBar.topAnchor, constant: -elementSpacing),
      
      title.bottomAnchor.constraint(equalTo: explanation.topAnchor, constant: -elementSpacing),
      title.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
      title.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor)
    ].forEach {
      $0.isActive = true
    }
  }
}

// MARK: - Styling

private extension SearchView {
  static func styleView(_ view: UIView) {
    view.backgroundColor = .systemBackground
  }
  
  static func styleTitle(_ label: UILabel) {
    label.text = "Poetic Pokemon"
    label.font = .systemFont(ofSize: 24, weight: .bold)
    label.textAlignment = .center
  }
  
  static func styleSubtitle(_ label: UILabel) {
    label.text = "Insert your Pokemon's name, and get a Shakespearean description."
    label.font = .systemFont(ofSize: 16)
    label.numberOfLines = 0
    label.textAlignment = .center
  }
  
  static func styleTextField(_ textField: UITextField) {
    textField.placeholder = "Pokemon name"
    textField.layer.borderWidth = 1
    textField.layer.borderColor = UIColor.systemGray.cgColor
    textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 40))
    textField.leftViewMode = .always
    textField.layer.cornerRadius = 20
    textField.clipsToBounds = true
  }
  
  static func styleButton(_ button: UIButton) {
    button.backgroundColor = .systemBlue
    button.setTitle("Search", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.setTitleColor(.systemGray, for: .disabled)
    button.layer.cornerRadius = 20
  }
}

// MARK: - Helpers

private extension SearchView {
  @objc
  func tappedActionButton() {
    Task {
      try? await viewModel.fetchData()
    }
  }
}
