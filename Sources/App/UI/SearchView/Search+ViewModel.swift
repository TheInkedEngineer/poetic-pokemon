//
//  Poetic Pokemon
//  Firas Safa
//

import Combine
import UIKit
import SwiftUI
import PoeticPokemonSDK

class SearchViewModel {
  
  // MARK: Properties
  
  /// The callable services of the `SDK`.
  let sdkServices: PoeticPokemon.Services
  
  /// A reference of the view controller to properly navigate.
  weak var routingViewController: UIViewController?
  
  /// The name of the `Pokemon` to lookup its data.
  @Published var pokemonName: String = ""
  
  /// The `URL` of the `Pokemon` sprite.
  @Published var spriteURL: URL?
  
  /// The description of the `Pokemon.`
  @Published var description: String = ""
  
  /// The possible error from the network request.
  @Published var error: PoeticPokemon.SDKError? = nil
  
  // MARK: Init
  
  init(
    sdkServices: PoeticPokemon.Services,
    routingViewController: UIViewController
  ) {
    self.sdkServices = sdkServices
    self.routingViewController = routingViewController
  }
  
  // MARK: Logic
  
  func fetchData() async throws {
    showLoader()
    do {
      description = try await sdkServices.getShakespeareanDescription(for: pokemonName)
      spriteURL = try await sdkServices.getSprite(for: pokemonName)
    } catch {
      hideLoader() { [weak self] in
        self?.showErrorAlert(for: error as! PoeticPokemon.SDKError)
      }
      
      return
    }
    
    hideLoader() { [weak self] in
      self?.showDetail()
    }
  }
  
  // MARK: Navigation
  
  // This is far from ideal.
  // However, implementing a coordinator pattern or a router is an overkill for this use case.
  // This is a good enough practice for a minimal project where the main focus is not the navigation.
  // In more structured projects I would opt for a custom tailored solution, including but not limited to
  // the previously mentioned patterns.
  
  func showLoader() {
    let viewController = LoaderViewController()
    viewController.modalPresentationStyle = .overFullScreen
    
    DispatchQueue.main.async { [weak self] in
      self?.routingViewController?.present(viewController, animated: false)
    }
  }
  
  func hideLoader(then completion: (() -> Void)? = nil) {
    DispatchQueue.main.async { [weak self] in
      self?.routingViewController?.presentedViewController?.dismiss(
        animated: false, completion: completion
      )
    }
  }
  
  func showDetail() {
    DispatchQueue.main.async { [weak self] in
      guard let self = self else {
        return
      }
      
      let swiftUIView = DetailView(
        detailViewModel: DetailViewModel(
          pokemonName: self.pokemonName, spriteURL: self.spriteURL!, description: self.description
        )
      )
      let hostingController = UIHostingController(rootView: swiftUIView)
      self.routingViewController?.present(hostingController, animated: true, completion: nil)
    }
  }
  
  func showErrorAlert(for error: PoeticPokemon.SDKError) {
    DispatchQueue.main.async { [weak self] in
      guard let self = self else {
        return
      }
      
      let viewController = UIAlertController(
        title: "Something went wrong",
        message: error.message,
        preferredStyle: .alert
      )
      
      viewController.addAction(UIAlertAction(title: "Ok", style: .cancel))
      self.routingViewController?.present(viewController, animated: true, completion: nil)
    }
  }
}
