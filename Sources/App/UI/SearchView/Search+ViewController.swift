//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit

class SearchViewController: BaseViewController {
  override func loadView() {
    view = SearchView(
      viewModel: SearchViewModel(
        sdkServices: dependencies.sdkServices,
        routingViewController: self
      )
    )
  }
}
