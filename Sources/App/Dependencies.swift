//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import PoeticPokemonSDK
import SwiftUI

class Dependencies {
  let sdkServices: PoeticPokemon.Services
  
  init() {
    sdkServices = PoeticPokemonSDK.PoeticPokemon(using: PoeticPokemonConfiguration()).services
  }
}

struct PoeticPokemonConfiguration: PoeticPokemonConfigurationProvider {
  var networkManagerConfiguration: NetworkManagerConfigurationProvider = NetworkManagerConfiguration()
  var visualConfiguration: VisualSettingsConfigurationProvider = VisualSettingsConfiguration()
}

struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
  var session: URLSession = URLSession(configuration: .ephemeral)
}

struct VisualSettingsConfiguration: VisualSettingsConfigurationProvider {
  var brandColor: Color = .red
  var textColor: Color = .white
  var font: Font = .subheadline
}
