//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit

extension Requests {
  /// Name space for all Pokemon related requests.
  enum Pokemon {}
}

extension Requests.Pokemon {
  /// Get the information related to a specific Pokemon.
  struct Information: DecodableRequest, Mockable {
    typealias ResponseModel = Models.Networking.Response.Pokemon.Information
    
    let baseURL: URL = BaseURL.pokemonAPI
    let method: HTTPMethod = .get
    var path: Path = ["pokemon-species"]
    
    /// Creates and returns a new instance.
    ///
    /// - Parameter name: The name of the Pokemon to fetch.
    init(_ name: String) {
      path.append(name.lowercased())
    }
  }
  
  /// Get the sprite associated with the Pokemon.
  struct Sprite: DecodableRequest, Mockable {
    typealias ResponseModel = Models.Networking.Response.Pokemon.Sprite
    
    let baseURL: URL = BaseURL.pokemonAPI
    let method: HTTPMethod = .get
    var path: Path = ["pokemon"]
    
    /// Creates and returns a new instance.
    ///
    /// - Parameter name: The name of the Pokemon to fetch.
    init(_ name: String) {
      path.append(name.lowercased())
    }
  }
}
