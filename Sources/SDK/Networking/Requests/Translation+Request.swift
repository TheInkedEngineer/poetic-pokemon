//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit

extension Requests {
  /// Name space for all translation related requests.
  enum Translate {}
}

extension Requests.Translate {
  /// Translates text into the Shakespearean style.
  struct Shakespeare: DecodableRequest, Mockable {
    typealias ResponseModel = Models.Networking.Response.Translation.Shakespeare
    
    let baseURL: URL = BaseURL.translationAPI
    let method: HTTPMethod = .post
    let path: Path = ["shakespeare"]
    let textToTranslate: String
    
    var parameters: Parameters {
      [ "text": textToTranslate ]
    }
    
    /// Creates and returns a new instance.
    ///
    /// - Parameter text: The text to translate to the Shakespearean style
    init(text: String) {
      textToTranslate = text
    }
  }
}
