//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import Logger
import Utils

/// The configuration needed to properly instantiate the network manager.
public protocol NetworkManagerConfigurationProvider {
  /// The `URLSession` instance to use when executing the data tasks.
  var session: URLSession { get }
}

struct NetworkManager {
  /// The instance of `URLSession` to use when making data requests.
  let session: URLSession
  
  /// Creates and returns a configured instance of `NetworkManager`.
  ///
  /// - Parameter configuration: An object conforming to `NetworkManagerConfigurationProvider` providing the necessary configuration to create the `NetworkManager` instance.
  init(with configuration: NetworkManagerConfigurationProvider) {
    session = configuration.session
  }
  
  /// Executes a network request, parses the response and returns it.
  ///
  /// - Parameter request: An instance of a `DecodableRequest`.
  ///
  /// - Returns: An instance the same type as the response model associated with the passed `DecodableRequest`.
  func request<R: DecodableRequest>(_ request: R) async throws -> R.ResponseModel {
    var data: Data!
    var response: URLResponse!
    var responseModel: R.ResponseModel!
    
    Logger.log(message: "Executing \(request)")
    
    do {
      (data, response) = try await session.data(for: request.asURLRequest())
    } catch {
      Logger.log(type: .error, message: error)
      
      guard let err = error as? URLError else {
        throw NetworkError.generic(error)
      }
      
      if err.code.rawValue == NSURLErrorCannotFindHost {
        throw NetworkError.hostNotFound
      }
      
      if err.code.rawValue == NSURLErrorNotConnectedToInternet {
        throw NetworkError.noInternetConnection
      }
      
      if err.code.rawValue == NSURLErrorTimedOut {
        throw NetworkError.requestTimedOut
      }
      
      throw NetworkError.generic(error)
    }
    
    guard let httpResponse = response as? HTTPURLResponse else {
      Logger.log(type: .error, message: "Was expecting HTTPURLResponse but got \(type(of: response)) instead)")
      throw NetworkError.serverResponseNotValid
    }
    
    if httpResponse.statusCode.between(400, and: 499) {
      Logger.log(type: .error, message: "Bad request. Got back \(httpResponse.statusCode)")
      
      if httpResponse.statusCode == 429 {
        throw NetworkError.tooManyRequests
      }
      
      if httpResponse.statusCode == 404 {
        throw NetworkError.resourceNotFound
      }
      
      throw NetworkError.clientError(httpResponse.statusCode)
    }
    
    if httpResponse.statusCode.between(500, and: 599) {
      Logger.log(type: .error, message: "Server crashed. Got back \(httpResponse.statusCode)")
      throw NetworkError.serverError(httpResponse.statusCode)
    }
    
    Logger.log(message: "Server responded with \(String(describing: String(data: data, encoding: .utf8)))")
    
    do {
      responseModel = try request.dataDecoder.decode(R.ResponseModel.self, from: data)
    } catch {
      Logger.log(
        type: .error,
        message: """
        Failed to decode \(String(describing: String(data: data, encoding: .utf8))) into \(R.ResponseModel.self)
        """
        )
      
      throw NetworkError.decodingError(data)
    }
    
    return responseModel
  }
}
