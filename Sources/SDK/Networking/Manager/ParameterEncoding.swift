//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

protocol ParameterEncoding {
  /// Encodes parameters inside of a `URLRequest`.
  ///
  /// - Parameters:
  ///   - urlRequest: `URLRequest` value onto which parameters will be encoded.
  ///   - parameters: `Parameters` to encode onto the request.
  ///   - encoder: `JSONEncoder` to use to encode onto the request.
  ///
  /// - Returns: The encoded `URLRequest`.
  ///
  /// - Throws:  `NetworkError.EncodingFailed` produced during parameter encoding.
  func encode(_ urlRequest: URLRequest, with parameters: Parameters, using encoder: JSONEncoder) -> URLRequest
}

