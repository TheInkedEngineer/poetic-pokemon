//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// Type representing HTTP methods.
enum HTTPMethod: String {
  /// `GET` method.
  case get = "GET"
  
  /// `GET` method.
  case post = "POST"
}
