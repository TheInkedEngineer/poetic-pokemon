//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A protocol used to define an `HTTP` request that expects a response that can be decoded into an  object.
protocol DecodableRequest: URLRequestConvertible, CustomStringConvertible {
  /// The expected model from the response.
  associatedtype ResponseModel: Decodable
  
  /// The `HTTP` method of the `API` call.
  var method: HTTPMethod { get }
  
  /// The base `URL` of the `API` call.
  var baseURL: URL { get }
  
  /// The path to the resource.
  var path: Path { get }
  
  /// The parameter's definition varies depending on the encoding.
  /// For `post`, `put` and `patch` requests, this usually means encoding these values in the body.
  /// Whereas for everything else, it usually means to encode them as part of the `URL`.
  /// Defaults to an empty dictionary.
  var parameters: Parameters { get }
  
  /// A type used to define how a set of parameters are applied to a `URLRequest`.
  /// Defaults to `URLEncoding.queryString` for `GET`, and `URLEncoding.httpBody` for `POST`.
  var encoding: EncodingMethod { get }
  
  /// The decoder to use when decoding response data.
  var dataDecoder: JSONDecoder { get }
  
  /// The encoder to use when encoding data to json.
  var dataEncoder: JSONEncoder { get }
  
  /// The desired Time before a timeout error is launched, in seconds.
  /// Defaults to 20s.
  var timeout: TimeInterval { get }
}

// MARK: - URLRequestConvertible Implementation

extension DecodableRequest {
  func asURLRequest() -> URLRequest {
    let url = path.reduce(baseURL) { newURL, nextPath -> URL in
      newURL.appendingPathComponent(nextPath)
    }
    
    var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeout)
    request.httpMethod = method.rawValue
    
    let encodedRequest = encoding.encode(request, with: parameters, using: dataEncoder)
    return encodedRequest
  }
}

// MARK: - Default Values

extension DecodableRequest {
  var encoding: EncodingMethod {
    switch method {
    case .get:
      return .queryString
      
    case .post:
      return .httpBody
    }
  }
  
  var timeout: TimeInterval {
    20
  }
  
  var parameters: Parameters {
    [:]
  }
  
  var dataDecoder: JSONDecoder {
    JSONDecoder()
  }
  
  var dataEncoder: JSONEncoder {
    JSONEncoder()
  }
}

// MARK: - Debug description

extension DecodableRequest {
  var description: String {
    """
    \(method.rawValue) request to URL: "\(baseURL.absoluteString)/\(path.joined(separator: "/"))
    With parameters \(parameters)
    """
  }
}
