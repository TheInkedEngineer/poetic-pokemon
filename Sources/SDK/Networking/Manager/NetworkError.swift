//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// The possible errors that can be thrown throughout the `Networking` library.
enum NetworkError: Error {
  /// The phone has no internet connection.
  case noInternetConnection
  
  /// The request has timed out.
  case requestTimedOut
  
  /// The server response was not an HTTPURLResponse
  case serverResponseNotValid
  
  /// The request encountered an error because of the client request.
  case clientError(_ code: Int)
  
  /// The endpoint refused to execute the request due to too many requests.
  case tooManyRequests
  
  /// The Pokemon resource could not be found.
  case resourceNotFound
  
  /// The request encountered an error because of the server.
  case serverError(_ code: Int)
  
  /// Could not decode the response as expected. The data is returned in the error.
  case decodingError(_ data: Data? = nil)
  
  /// A generic unmanaged error.
  case generic(_ error: Error)
  
  /// The host address could not be found.
  /// This is necessary since the fun translation API often does not resolve.
  case hostNotFound
}
