//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// The list of supported encoding methods.
enum EncodingMethod: ParameterEncoding {
  /// Encodes data at the end of the `URL` as a query string.
  case queryString
  
  /// Encodes data in the body of the request.
  case httpBody
  
  // This has been implemented with a "good enough" approach.
  // This is all we need for the current use case.
  // This can be developed in a more complete solution, but it would be an overkill for now.
  func encode(
    _ urlRequest: URLRequest,
    with parameters: Parameters,
    using encoder: JSONEncoder
  ) -> URLRequest {
    var request = urlRequest
    
    switch self {
    case .queryString:
    // There is no use case currently for a query string, therefore it hasn't been implemented.
    break
      
    case .httpBody:
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      request.httpBody = try? encoder.encode(parameters)
    }
    
    return request
  }
}
