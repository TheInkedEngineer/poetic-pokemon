//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// An object that can generate a `URLRequest`.
protocol URLRequestConvertible {
  /// Generates a `URLRequest`.
  ///
  /// - Returns: a properly formed `URLRequest`
  func asURLRequest() -> URLRequest
}
