//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// An object representing an empty response for an `DecodableRequest`.
struct Empty: Decodable {}
