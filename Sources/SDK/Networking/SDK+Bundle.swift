//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

extension Bundle {
  /// The `Networking` bundle.
  static let sdk = Bundle(identifier: "com.theinkedengineer.PoeticPokemonSDK")!
}
