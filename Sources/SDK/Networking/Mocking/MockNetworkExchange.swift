//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

struct MockNetworkExchange: Hashable {
  
  // MARK: Properties
  
  /// The `URLRequest` associated to the request.
  let urlRequest: URLRequest
  
  /// The mocked response inside of the exchange.
  let response: MockResponse?
  
  /// An optional error
  let error: MockError?
  
  /// The expected `HTTPURLResponse`.
  var urlResponse: HTTPURLResponse? {
    guard let response = response else {
      return nil
    }
    
    return HTTPURLResponse(
      url: urlRequest.url!,
      statusCode: response.statusCode.rawValue,
      httpVersion: response.httpVersion.rawValue,
      // Merges existing headers, if any, with the custom mock headers favoring the latter.
      headerFields: (urlRequest.allHTTPHeaderFields ?? [:]).merging(response.headers) { $1 }
    )
  }
  
  // MARK: Init
  
  init(urlRequest: URLRequest, response: MockResponse?, error: MockError? = nil) {
    self.urlRequest = urlRequest
    self.response = response
    self.error = error
  }
}
