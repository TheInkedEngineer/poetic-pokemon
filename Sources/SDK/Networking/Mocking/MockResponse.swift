//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

struct MockResponse: Hashable {
  /// The desired status code to expect from the request.
  let statusCode: MockStatusCode

  /// The desired http version to include in the response.
  let httpVersion: MockHTTPVersion
  
  /// The expected response data, if any.
  let data: Data?

  /// Custom headers to add to the mocked response.
  let headers: [String: String]
  
  init(
    statusCode: MockStatusCode = .code200,
    httpVersion: MockHTTPVersion = .onePointOne,
    data: Data? = nil,
    headers: [String: String] = [:]
  ) {
    self.statusCode = statusCode
    self.httpVersion = httpVersion
    self.data = data
    self.headers = headers
  }
}
