//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// The list of supported HTTP versions for mocking.
enum MockHTTPVersion: String {
  case onePointOne = "HTTP/1.1"
}
