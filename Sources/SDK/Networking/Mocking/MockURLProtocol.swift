//
//  Poetic Pokemon
//  Firas Safa
//

import UIKit
import Logger

/// A subclass of `URLProtocol` that allows us to mock network exchanges to simulate different request/response combos.
class MockURLProtocol: URLProtocol {
  
  // MARK: - Properties
  
  /// The set containing all the requests we would want to mock.
  static var mockRequests: Set<MockNetworkExchange> = []
  
  /// Whether or not to check the query parameters when trying to find a request in the set.
  /// If `false` only the base path will be checked regardless of the query parameters.
  static var shouldCheckQueryParameters = false
  
  // MARK: - Methods

  override class func canInit(with request: URLRequest) -> Bool {
    true
  }
  
  override class func canonicalRequest(for request: URLRequest) -> URLRequest {
     request
  }
  
  override func startLoading() {
    // Tells the client that the protocol implementation has finished loading.
    defer {
      client?.urlProtocolDidFinishLoading(self)
    }
    
    Logger.log(
      message: """
      \(Self.self) - Searching for:
      \(String(describing: request.httpMethod)) - \(String(describing: request.url?.path))
      """
    )
    
    let foundRequest = Self.mockRequests.first { [unowned self] in
      request.url?.path == $0.urlRequest.url?.path &&
        request.httpMethod == $0.urlRequest.httpMethod &&
        (Self.shouldCheckQueryParameters ? request.url?.query == $0.urlRequest.url?.query : true)
    }
    
    if let error = foundRequest?.error {
      client?.urlProtocol(self, didFailWithError: error.urlError)
      return
    }
    
    guard let mockExchange = foundRequest else {
      client?.urlProtocol(self, didFailWithError: MockError.routeNotFound)
      
      Logger.log(
        type: .error,
        message: """
        \(Self.self) - Could NOT find:
        \(String(describing: request.httpMethod)) - \(String(describing: request.url?.path))
        """
      )
      
      return
    }
    
    Logger.log(
      message: """
      \(Self.self) - Found:
      \(String(describing: request.httpMethod)) - \(String(describing: request.url?.path))
      """
    )
    
    if let data = mockExchange.response?.data {
      client?.urlProtocol(self, didLoad: data)
    }
    
    guard let response = mockExchange.urlResponse else {
      return
    }
    
    client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
  }
  
  override func stopLoading() {}
}
