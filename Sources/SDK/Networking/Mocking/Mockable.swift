//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A request is mockable
protocol Mockable {
  /// A `MockNetworkExchange` generator.
  ///
  /// - Parameters:
  ///   - urlRequest: `URLRequest` value to pass to the `MockNetworkExchange` from which the `URL` is extracted.
  ///   - statusCode: `MockStatusCode` value to return when the request is called.
  ///   - httpVersion: `MockHTTPVersion` value to return when the request is called.
  ///   - headers: Headers values to add to the request.
  ///   - mockDataFile: `RawRepresentable` value with the file name from which to extract the data returned from the request.
  ///
  /// - Returns: `MockNetworkExchange` configured based on the passed data.
  static func mockNetworkExchange<R: RawRepresentable> (
    urlRequest: URLRequest,
    statusCode: MockStatusCode,
    httpVersion: MockHTTPVersion,
    headers: [String: String],
    mockDataFile: R
  ) -> MockNetworkExchange where R.RawValue == String
}

// MARK: - Implementation

extension Mockable {
  static func mockNetworkExchange<R: RawRepresentable>(
    urlRequest: URLRequest,
    statusCode: MockStatusCode = .code200,
    httpVersion: MockHTTPVersion = .onePointOne,
    headers: [String: String] = [:],
    mockDataFile: R
  ) -> MockNetworkExchange where R.RawValue == String {
    guard let filePath = Bundle.sdk.path(forResource: mockDataFile.rawValue, ofType: "json") else {
      return MockNetworkExchange(
        urlRequest: urlRequest,
        response: MockResponse(
          statusCode: .code404,
          httpVersion: httpVersion,
          data: nil,
          headers: [:]
        )
      )
    }
    
    guard
      let content = try? String(contentsOfFile: filePath),
      let data = content.data(using: .utf8)
    else {
      // Virtually impossible to happen and therefore impossible to test.
      return MockNetworkExchange(
        urlRequest: urlRequest,
        response: MockResponse(
          statusCode: .code500,
          httpVersion: httpVersion,
          data: nil,
          headers: [:]
        )
      )
    }
    
    return MockNetworkExchange(
      urlRequest: urlRequest,
      response: MockResponse(
        statusCode: statusCode,
        httpVersion: httpVersion,
        data: data,
        headers: [:]
      )
    )
  }
}
