//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A list of all the status codes we want to simulate in a Mock Request.
enum MockStatusCode: Int {
  /// OK
  ///
  /// The request has succeeded.
  case code200 = 200
  
  /// FORBIDDEN
  ///
  /// The server understood the request, but is refusing to fulfill it.
  case code403 = 403
  
  /// NOT FOUND
  ///
  /// The desired resource could not be found.
  case code404 = 404
  
  /// TOO MANY REQUESTS
  ///
  /// The user has sent too many requests in a given amount of time ("rate limiting").
  case code429 = 429
  
  /// INTERNAL SERVER ERROR.
  ///
  /// Something went severely wrong with the server.
  case code500 = 500
}
