//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

enum MockError: Int, Error, Hashable, Equatable {
  case notConnectedToInternet
  case requestTimedOut
  case routeNotFound
  case hostNotFound
  
  var urlError: URLError {
    switch self {
    case .notConnectedToInternet:
      return URLError(.notConnectedToInternet)
      
    case .requestTimedOut:
      return URLError(.timedOut)
      
    case .routeNotFound:
      return URLError(.resourceUnavailable)
      
    case .hostNotFound:
      return URLError(.cannotFindHost)
    }
  }
}
