//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

enum MockDataFile: String {
  /// `Shakespearean.json` file.
  case shakespearean = "Shakespearean"
  
  /// `Pokemon.json` file.
  case pokemon = "Pokemon"
  
  /// `PokemonSpecies.json` file.
  case pokemonSpecies = "PokemonSpecies"
}
