//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A dictionary representing headers in an `HTTP` request. The `key` and `value` are `String`.
typealias HTTPHeaders = [String: String]

/// A dictionary holding `key` - `value` pairs using to encode data in `HTTP` requests.
typealias Parameters = [String: String]

/// A path to a resource at the end of a URL.
/// This is represented with an array of `String`s where each `String` is a sub path.
typealias Path = [String]
