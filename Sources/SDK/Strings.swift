//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

enum TextContent {
  enum Error {
    /// The resource for this Pokemon could not be found. Make sure the name is spelled correctly.
    static let resourceNotFound = "The resource for this Pokemon could not be found. Make sure the name is spelled correctly."
    
    /// You have reached your search limit, please try again later.
    static let tooManyRequests = "You have reached your search limit, please try again later."
    
    /// Something went fatally wrong. We apologize for the inconvenience.
    static let fatal = "Something went fatally wrong. We apologize for the inconvenience."
    
    /// Something we wrong with our API provider. We apologize for the inconvenience. Try again later.
    static let providerError = "Something we wrong with our API provider. We apologize for the inconvenience. Try again later."
    
    /// There were some connection issues. Make sure you are connected to the internet and have a strong signal.
    static let connectionIssues = "There were some connection issues. Make sure you are connected to the internet and have a strong signal."
    
    /// No description was found for this Pokemon.
    static let noDescriptionFound = "No description was found for this Pokemon."
  }
}
