//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// Name space for the all the models.
enum Models {
  /// A name space for all the models in the `SDK`.
  enum SDK {}
  
  /// A name space for all the models in the relating to the networking layer.
  enum Networking {}
}

extension Models.Networking {
  /// A name space for all the models of the networking responses.
  enum Response {}
}
