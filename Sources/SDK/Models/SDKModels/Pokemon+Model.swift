//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

extension Models.SDK {
  /// Name space for all Pokemon related models.
  enum Pokemon {}
}

extension Models.SDK.Pokemon {
  struct Information: Decodable {
    /// The description of the Pokemon.
    let description: String
  }
  
  struct Sprite: Decodable {
    /// The `URL` of the sprite.
    let url: URL
  }
}
