//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

extension Models.SDK {
  /// Name space for all Translation related models.
  enum Translation {}
}

extension Models.SDK.Translation {
  struct Shakespeare: Decodable {
    /// The text translated to the Shakespearean style.
    let text: String
  }
}
