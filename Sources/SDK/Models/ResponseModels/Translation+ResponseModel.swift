//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

extension Models.Networking.Response {
  /// Name space for all Pokemon related requests.
  enum Translation {}
}

extension Models.Networking.Response.Translation {
  struct Shakespeare: Decodable, Normalizable {
    
    /// The content of the translation response.
    struct Content: Decodable {
      /// The value of the `flavor_text`.
      let translated: String
    }
    
    // MARK: Coding Keys
    
    enum CodingKeys: String, CodingKey {
      case content = "contents"
    }
    
    // MARK: Properties
    
    /// All the available descriptions of the Pokemon.
    let content: Content
  }
}

// MARK: - Normalization

extension Models.Networking.Response.Translation.Shakespeare {
  func normalize() -> Models.SDK.Translation.Shakespeare {
    Models.SDK.Translation.Shakespeare(
      text: content.translated
    )
  }
}
