//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

extension Models.Networking.Response {
  /// Name space for all Pokemon related requests.
  enum Pokemon {}
}

extension Models.Networking.Response.Pokemon {
  struct Information: Decodable, Normalizable {
    
    // MARK: Data Structure
    
    /// The single `flavor_text` entry.
    struct Entry: Decodable {
      enum CodingKeys: String, CodingKey {
        case text = "flavor_text"
        case language
      }
      
      /// The value of the `flavor_text`.
      let text: String

      /// The language of the entry.
      let language: Language
    }
    
    /// The language block associated with the description.
    struct Language: Decodable {
      /// The name of the language
      let name: String
    }
    
    // MARK: Coding Keys
    
    enum CodingKeys: String, CodingKey {
      case descriptions = "flavor_text_entries"
    }
    
    // MARK: Properties
    
    /// All the available descriptions of the Pokemon.
    let descriptions: [Entry]
  }
  
  struct Sprite: Decodable, Normalizable {
    
    // MARK: Coding Keys
    
    enum CodingKeys: String, CodingKey {
      case sprites
    }
    
    enum SpritesCodingKeys: String, CodingKey {
      case front = "front_default"
    }
    
    // MARK: Properties
    
    /// The `URL` where the sprite resides.
    let url: URL
    
    // MARK: Custom Decoding
    
    init(from decoder: Decoder) throws {
      let outerContainer = try decoder.container(keyedBy: CodingKeys.self)
      let spritesContainer = try outerContainer.nestedContainer(keyedBy: SpritesCodingKeys.self, forKey: .sprites)
      
      url = try spritesContainer.decode(URL.self, forKey: .front)
    }
  }
}

// MARK: - Normalization

extension Models.Networking.Response.Pokemon.Information {
  func normalize() -> Models.SDK.Pokemon.Information {
    Models.SDK.Pokemon.Information(
      description: descriptions
        .filter { $0.language.name == "en" } // Filter only english ones
        .randomElement()?
        .text
        .replacingOccurrences(of: "\n", with: " ") ?? TextContent.Error.noDescriptionFound
    )
  }
}

extension Models.Networking.Response.Pokemon.Sprite {
  func normalize() -> Models.SDK.Pokemon.Sprite {
    Models.SDK.Pokemon.Sprite(url: url)
  }
}
