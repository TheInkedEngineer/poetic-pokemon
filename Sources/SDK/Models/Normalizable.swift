//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A `Normalizable` object is a `Decodable` object, usually a response from a network call,
/// that can be transformed into an `SDKModel` to be used around in the application.
/// This separation layer guarantees that the SDK model are rarely if ever touched,
/// and any changes to the API response model is dealt with in the response model, and the `normalize()` function
/// makes sure that the changes do not affect the current state of the `SDK` model.
protocol Normalizable where Self: Decodable {
  associatedtype Output
  
  /// Transforms an object into another based on the SDK needs.
  /// - Returns: An `SDK` model to use around the application.
  func normalize() -> Output
}
