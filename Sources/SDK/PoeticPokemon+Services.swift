//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

public extension PoeticPokemon {
  /// The access point to all the `API` services related to the `PoeticPokemon` `SDK`.
  struct Services {
    
    // MARK: - Properties
    
    /// The network manager instance used to execute network calls.
    private let networkManager: NetworkManager
    
    // MARK: - Init
    
    init(using configuration: NetworkManagerConfigurationProvider) {
      networkManager = NetworkManager(with: configuration)
    }
    
    // MARK: - Methods
    
    /// Fetches the `Shakespearean` description for a given Pokemon name.
    /// It randomly takes one of the english provided flavor text entries and transforms in into the Shakespearean style.
    ///
    /// - Parameter pokemon: The name of the Pokemon to lookup.
    ///
    /// - Throws: `SDKError`.
    ///
    /// - Returns: The `Shakespearean` description.
    public func getShakespeareanDescription(for pokemon: String) async throws -> String {
      do  {
        let normalDescriptionRequest = Requests.Pokemon.Information(pokemon)
        
        let normalDescription = try await networkManager
          .request(normalDescriptionRequest)
          .normalize()
          .description
        
        let shakespeareanDescriptionRequest = Requests
          .Translate
          .Shakespeare(text: normalDescription)
        
        let shakespeareanDescription = try await networkManager
          .request(shakespeareanDescriptionRequest)
          .normalize()
          .text
        
        return shakespeareanDescription
      } catch {
        throw managerError(error)
      }
    }
    
    /// Fetches the sprite associated with a Pokemon.
    ///
    /// - Parameter pokemon: The name of the Pokemon.
    ///
    /// - Throws: `SDKError`.
    ///
    /// - Returns: The `URL` of the sprite.
    public func getSprite(for pokemon: String) async throws -> URL {
      let request = Requests.Pokemon.Sprite(pokemon)
      
      do {
        return try await networkManager.request(request).normalize().url
      } catch {
        throw managerError(error)
      }
    }
    
    /// Transforms the `Error` into an `SDKError`.
    ///
    /// - Parameter error: The error to transforms. Ideally, a `NetworkError`.
    ///
    /// - Returns: `SDKError` value.
    func managerError(_ error: Error) -> SDKError {
      guard let err = error as? NetworkError else {
        return SDKError.fatal
      }
      
      switch err {
      case .serverResponseNotValid, .serverError, .decodingError, .hostNotFound:
        return SDKError.providerError
        
      case .resourceNotFound:
        return SDKError.resourceNotFound
        
      case .tooManyRequests:
        return SDKError.tooManyRequests
        
      case .noInternetConnection, .requestTimedOut:
        return SDKError.connectionIssues
        
      case .generic, .clientError:
        return SDKError.fatal
      }
    }
  }
}
