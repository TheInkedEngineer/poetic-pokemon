//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// An object containing all the configuration needed by the `SDK` to properly function.
public protocol PoeticPokemonConfigurationProvider {
  var networkManagerConfiguration: NetworkManagerConfigurationProvider { get }
  var visualConfiguration: VisualSettingsConfigurationProvider { get }
}
