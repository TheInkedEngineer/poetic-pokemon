//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

enum BaseURL {
  /// API endpoint to lookup a Pokemon by name.
  static let pokemonAPI = URL(string: "https://pokeapi.co/api/v2")!
  
  /// API endpoint to translate a text.
  static let translationAPI = URL(string: "https://api.funtranslations.com/translate")!
}
