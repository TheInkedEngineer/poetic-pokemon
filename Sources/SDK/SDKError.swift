//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

public extension PoeticPokemon {
  /// A name space for all the `Errors` generated by the `PoeticPokemon` `SDK`.
  enum SDKError: Error {
    /// The resource of the `Pokemon` could not be found.
    case resourceNotFound
    
    /// The translation API returned `too many requests`.
    case tooManyRequests
    
    /// The request could not succeed due to connection issues.
    case connectionIssues
    
    /// Something unexpected when severely wrong.
    case fatal
    
    /// An error occurred due to the API providers and is out of our control. Like error 500 for instance.
    case providerError
    
    /// Human readable message of the error.
    public var message: String {
      switch self {
      case .resourceNotFound:
        return TextContent.Error.resourceNotFound

      case .connectionIssues:
        return TextContent.Error.connectionIssues
      
      case .tooManyRequests:
        return TextContent.Error.tooManyRequests
        
      case .providerError:
        return TextContent.Error.providerError
        
      case .fatal:
        return TextContent.Error.fatal
      }
    }
  }
}
