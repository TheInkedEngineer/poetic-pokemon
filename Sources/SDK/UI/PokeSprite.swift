//
//  Poetic Pokemon
//  Firas Safa
//

import SwiftUI

public extension PoeticPokemon.UI {
  /// A `View` that asynchronously loads an image given its `URL`.
  /// While loading it displays a loader.
  /// The image has a width and height equal to 150.
  struct PokeSprite: View {
    let url: URL
    
    public init(url: URL) {
      self.url = url
    }
    
    public var body: some View {
      VStack {
        AsyncImage(url: url) { image in
          image
            .resizable()
            .scaledToFill()
        } placeholder: {
          ProgressView()
            .frame(width: 150, height: 150)
            .cornerRadius(75)
        }
      }
      .padding()
      .background(PoeticPokemon.UI.visualConfiguration.brandColor)
      .frame(width: 150, height: 150)
      .cornerRadius(75)
      .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 2)
      .shadow(color: PoeticPokemon.UI.visualConfiguration.brandColor.opacity(0.3), radius: 20, x: 0, y: 10)
    }
  }

}
