//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import SwiftUI

public protocol VisualSettingsConfigurationProvider {
  /// The color of the brand, which will be reflected as a background color for the UI components.
  var brandColor: Color { get }
  
  /// The color of the text inside the PoemView.
  var textColor: Color { get }
  
  /// The font of the text.
  var font: Font { get }
}
