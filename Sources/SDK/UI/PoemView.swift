//
//  Poetic Pokemon
//  Firas Safa
//

import SwiftUI

public extension PoeticPokemon.UI {
  /// A card element that given a text automatically adapts to that text and displays it in white with a green background.
  struct PoemCard: View {
    let text: String
    
    public init(text: String) {
      self.text = text
    }
    
    public var body: some View {
      VStack {
        Text(text)
          .font(PoeticPokemon.UI.visualConfiguration.font)
          .bold()
          .padding()
          .foregroundColor(PoeticPokemon.UI.visualConfiguration.textColor)
          .shadow(color: .white, radius: 20)
      }
      .padding()
      .background(PoeticPokemon.UI.visualConfiguration.brandColor)
      .cornerRadius(20)
      .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 2)
      .shadow(color: .green.opacity(0.3), radius: 20, x: 0, y: 10)
    }
  }
}
