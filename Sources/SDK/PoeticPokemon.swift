//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import SwiftUI

/// A  namespace for the `PoeticPokemon` `SDK`.
public struct PoeticPokemon {
  
  // MARK: Properties
  
  /// The `PoeticPokemon.Services` containing all the `API`s provided by the `SDK`
  public let services: PoeticPokemon.Services
  
  // MARK: Init
  
  public init(using configuration: PoeticPokemonConfigurationProvider) {
    services = PoeticPokemon.Services(using: configuration.networkManagerConfiguration)
    PoeticPokemon.UI.visualConfiguration = configuration.visualConfiguration
  }
}

// MARK: - Namespace Extensions

public extension PoeticPokemon {
  /// A name space for all the `UI` elements related to the `PoeticPokemon` `SDK`.
  struct UI {
    static var visualConfiguration: VisualSettingsConfigurationProvider!
  }
}
