//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// The supported log types.
public enum LogType: String {
  /// Debugging code. Nothing to worry about.
  case debug = "ℹ️"

  /// An error has been encountered that needs addressing.
  case error = "❌"
}
