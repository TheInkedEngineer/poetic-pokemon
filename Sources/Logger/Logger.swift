//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

/// A name space for the logger.
public enum Logger {}

public extension Logger {
  /// Logs to the console a properly formatted debug message.
  /// 
  /// - Parameters:
  ///   - type: The type of the log. Defaults to `debug`.
  ///   - message: The content of the log.
  ///   - file: The file where the log was from.
  ///   - line: The line where the log was from.
  static func log(
    type: LogType = .debug,
    message: Any?,
    file: StaticString = #file,
    line: UInt = #line
  ) {
    #if DEBUG
    print("===== LOG START =====")
    print("From: \(file), line \(line)")
    print("\(type.rawValue) \(message ?? String(describing: message))")
    print("===== END LOG =====")
    #endif
  }
}
