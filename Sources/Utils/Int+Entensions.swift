//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation

public extension Int {
  /// Checks whether an integer is equal or higher than a lower boundary or equal or lower than a higher boundary.
  ///
  /// The low boundary has to be less or equal to the high boundary.
  /// - Parameters:
  ///   - low: The low boundary to check against.
  ///   - high: The hight boundary to check against.
  ///
  /// - Returns: A boolean value.
  func between(_ low: Self, and high: Self) -> Bool {
    assert(low <= high, "low has to be less or equal to high")
    
    if self == low || self == high {
      return true
    }
    
    return self > low && self < high
  }
}
