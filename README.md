#  Poetic Pokemon

## Overview
- The project has one external dependencies, used for UI tests, [ViewInspector](https://github.com/nalexn/ViewInspector)
- The project is generated using [Xcodegen](https://github.com/yonaskolb/XcodeGen).
- A `setup.sh` file exists that automatically installs everything necessary (Homebrew, Xcodegen..) and runs the necessary commands to generate the xcodeproj.
- This project is made of two parts:
	- An `SDK` that provides two services, and two UI components.
	- An application that consumes that `SDK` features.
- It has a minimum target of iOS 15 since it uses Swift 5.5 concurrency features.
- I purposely mixed different technologies because I believe the right tool should be used for the right job.

## App

The application is built using the MVVM architecture, leveraging Apple's `Combine` reactive framework. The main view is built using `UIKit` and the detail view is built using `SwiftUI` being a container of two other views provided by the `SDK`. Errors are displayed using a native `UIAlertController`.

The main view displays a search bar and button. The button is activated after at least 3 letters are inserted since no Pokemon has a shorter name. When the button is tapped, the requests are made, if they succeed, the detail view is modally displayed, otherwise an error is shown to the user.

## SDK

The `SDK` is configurable, and therefore requires an object conforming to `PoeticPokemonConfigurationProvider` when instantiated. This object will configure the `SDK`'s network layer and UI appearance.

Below an example on how to configure the `SDK`:

```swift
struct PoeticPokemonConfiguration: PoeticPokemonConfigurationProvider {
  var networkManagerConfiguration: NetworkManagerConfigurationProvider = NetworkManagerConfiguration()
  var visualConfiguration: VisualSettingsConfigurationProvider = VisualSettingsConfiguration()
}

struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
  var session: URLSession = URLSession(configuration: .ephemeral)
}

struct VisualSettingsConfiguration: VisualSettingsConfigurationProvider {
  var brandColor: Color = .red
  var textColor: Color = .white
  var font: Font = .subheadline
}
```

### UI

The `SDK` offers 2 UI components built using SwiftUI.

- `PoemView` is a card that displays the text its fed in a beautiful way with some nice shadows around.
- `PokeSprite` is a circular view that displays the image downloaded from a fed URL. It leverage's `SwiftUI` `AsyncImage`.

Both components are customizable and adapt to colors provided in the configuration phase.

Even though they are built using `SwiftUI`, they area easy to integrate with UIKit` using the example below.

```swift
let childView = UIHostingController(rootView: PoeticPokemon.UI.PoemView())
addChild(childView)
childView.view.frame = frame
view.addSubview(childView.view)
childView.didMove(toParent: self)
```


### Services

The `SDK` exposes two main methods.

```swift
func getShakespeareanDescription(for pokemon: String) async throws -> String
```

and 

```swift
func getSprite(for pokemon: String) async throws -> URL
```  

The former fetches and translates the description of a Pokemon, whereas the latter fetches the `URL` where the Sprite of the Pokemon resides. Both are async methods methods and can throw one of the `SDKError`.

When a function is called, the request is created, it is then fed to the network manager which makes the call and parses it, returns it to the service which is its turn extract the necessary information and returns it to the called.

This is made possible thanks to some powerful tooling built with ease of use in mind.

#### DecodableRequest

A protocol that makes creating a `URLRequest` easy and fast. Default values makes it even more easier to work with. Below is an example of a typical request:

```swift
  struct Sprite: DecodableRequest {
    typealias ResponseModel = Models.Networking.Response.Pokemon.Sprite
    
    let baseURL: URL = BaseURL.pokemonAPI
    let method: HTTPMethod = .get
    var path: Path = ["pokemon"]
    
    init(_ name: String) {
      path.append(name.lowercased())
    }
  }
```

- `ResponseModel` is a decodable model that helps the network manager know how to decode the received answer
- `baseURL`, `method` and `path` makes it possible to automatically generate the `URLRequest` using the `asURLRequest` method that is automatically implemented. 

#### Network Manager

The network manager is responsible for all the networking inside of the `SDK`. It exposes one asynchronous method:

```
func request<R: DecodableRequest>(_ request: R) async throws -> R.ResponseModel
```

This method makes a network call, fetches the data and decodes it based on the request's response model. It manages possible network errors and throws the correct error accordingly.

#### MockURLProtocol

`MockURLProtocol` is a subclass of `URLProtocol`. It allows mocking all network requests and test out how they would behavior in different scenarios.Be that simulating errors, different status codes, and different answers. It allows for great testability, in fact using `MockURLProtocol` I managed to test all requests, their responses and errors using `MockURLProtocol`, and it makes it possible to eventually use the application offline, if needed, or to mock  network calls before the backend builds the actual service. A detailed explanation can be found [here](https://www.theinkedengineer.com/articles/mocking-requests-using-url-protocol/) in an article I wrote on my blog.

### Models

There are 2 types of Models. Response models which are mapped 1:1 with the JSON returned from answers, and App models which are models mapped as they are needed to be represented by the app. The transition between one and the other is done by conforming the response model to `Normalizable` and transforming it to a model that the app can consume.
This guarantees that any change to the API in the future can be dealt with in a single place `func normalize() -> Output` without having to necessarily touch the App models.

## Testability

The core of the `SDK` is fully tested. Extreme edge cases are not and some initializers are not since they were too hard to test, and not worth the effort given the scenario. The `SDK` UI has some XCTests. Generally the core is tested around 98% and the SDK as a whole around 90%. There are over 50 different tests.

## WARNING

The fun translation API endpoint is not reliable. Often I run into this issue:

```
Executing POST request to URL: "https://api.funtranslations.com/translate/shakespeare

With parameters ["text": "This Pikachu is wearing its Trainer’s cap. The cap is proof that the two traveled across many regions together."]


2021-10-16 23:48:43.560627+0200 PoeticPokemon[31886:901048] Connection 2: received failure notification
2021-10-16 23:48:43.560746+0200 PoeticPokemon[31886:901048] Connection 2: failed to connect 12:8, reason -1
2021-10-16 23:48:43.560938+0200 PoeticPokemon[31886:901048] Connection 2: encountered error(12:8)
2021-10-16 23:48:43.562408+0200 PoeticPokemon[31886:901049] Task <E4212DD4-D485-4C01-8A9B-61FD5EC28026>.<4> HTTP load failed, 0/0 bytes (error code: -1003 [12:8])
2021-10-16 23:48:43.575951+0200 PoeticPokemon[31886:901048] Task <E4212DD4-D485-4C01-8A9B-61FD5EC28026>.<4> finished with error [-1003] Error Domain=NSURLErrorDomain Code=-1003 "A server with the specified hostname could not be found." UserInfo={_kCFStreamErrorCodeKey=8, NSUnderlyingError=0x600003826b50 {Error Domain=kCFErrorDomainCFNetwork Code=-1003 "(null)" UserInfo={_kCFStreamErrorCodeKey=8, _kCFStreamErrorDomainKey=12}}, _NSURLErrorFailingURLSessionTaskErrorKey=LocalDataTask <E4212DD4-D485-4C01-8A9B-61FD5EC28026>.<4>, _NSURLErrorRelatedURLSessionTaskErrorKey=(
    "LocalDataTask <E4212DD4-D485-4C01-8A9B-61FD5EC28026>.<4>"
), NSLocalizedDescription=A server with the specified hostname could not be found., NSErrorFailingURLStringKey=https://api.funtranslations.com/translate/shakespeare, NSErrorFailingURLKey=https://api.funtranslations.com/translate/shakespeare, _kCFStreamErrorDomainKey=12}
```

It is properly managed, and displays that something wrong with the API provider, however I thought it was better to explicitly mention it.
