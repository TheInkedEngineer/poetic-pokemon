#!/bin/bash
set -e

PROJECT_NAME="PoeticPokemon"

# Install Homebrew dependency manager. 
if [[ ! -x "$(command -v brew)" ]]; then
  echo 'Homebrew is not installed.' >&2

  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Installs XcodeGen to generate the xcodeproj.
if ! [[ -x "$(command -v xcodegen)" ]]; then
  echo 'XcodeGen is not installed.' >&2
  brew install xcodegen
fi

for var in $@
do
  # If argument "close" is passed, close the project.
  if [[ "$var" == "close" ]]; then
    echo -e "Killing Xcode."
    osascript -e 'tell app "Xcode" to quit'
  fi

  # If "pod-remove" argument is passed, delete the Pods folder.
  if [[ "$var" == "pod-remove" ]]; then
    echo -e "Deleting Pods folder."
    rm -rf Pods
  fi

  # If "pod-update" argument is passed, updates the dependencies.
  if [[ "$var" == "pod-update" ]]; then
    echo -e "Running pod update to update pods."
    pod update
  fi
done

# Removes the old project and workspace if present.
rm -rf $PROJECT_NAME.xcodeproj
rm -rf $PROJECT_NAME.xcworkspace

# Create the new project.
xcodegen

# Install pods
# pod install

# Move config files to right place.
cp Configurations/Xcode/IDETemplateMacros.plist $PROJECT_NAME.xcodeproj/xcshareddata/IDETemplateMacros.plist


for var in $@
do
  # If "open" argument is passed, open the project.
  if [[ "$var" == "open" ]]; then
    echo -e "Opening project."
    open $PROJECT_NAME.xcodeproj
  fi
done
