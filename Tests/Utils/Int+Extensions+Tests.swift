//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import Utils

class TestIntExtensions: XCTestCase {
  func testBetweenTwoNumbers() {
    XCTAssertTrue(10.between(5, and: 10))
    XCTAssertTrue(10.between(10, and: 20))
    XCTAssertTrue(10.between(5, and: 20))
    XCTAssertFalse(10.between(5, and: 7))
    XCTAssertFalse(10.between(11, and: 17))
  }
}
