//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import SwiftUI
import XCTest

@testable import PoeticPokemonSDK

class PoeticPokemonTests: XCTestCase {
  struct PoeticPokemonConfiguration: PoeticPokemonConfigurationProvider {
    var networkManagerConfiguration: NetworkManagerConfigurationProvider = NetworkManagerConfiguration()
    var visualConfiguration: VisualSettingsConfigurationProvider = VisualSettingsConfiguration()
  }

  struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
    var session: URLSession = URLSession(configuration: .ephemeral)
  }

  struct VisualSettingsConfiguration: VisualSettingsConfigurationProvider {
    var brandColor: Color = .red
    var textColor: Color = .white
    var font: Font = .subheadline
  }
  
  func testUIConfiguredCorrectly() {
    let _ = PoeticPokemon(using: PoeticPokemonConfiguration())
    XCTAssertEqual(PoeticPokemon.UI.visualConfiguration.brandColor, .red)
    XCTAssertEqual(PoeticPokemon.UI.visualConfiguration.textColor, .white)
    XCTAssertEqual(PoeticPokemon.UI.visualConfiguration.font, .subheadline)
  }
}
