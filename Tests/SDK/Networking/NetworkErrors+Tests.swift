//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class TestNetworkErrors: XCTestCase {
  
  private var sessionConfiguration: URLSessionConfiguration = {
    let configuration = URLSessionConfiguration.default
    configuration.protocolClasses = [MockURLProtocol.self]
    
    return configuration
  }()
  
  private struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
    let session: URLSession
  }
  
  private struct DemoGetRequest: DecodableRequest {
    typealias ResponseModel = Empty
    
    let method: HTTPMethod = .get
    let baseURL: URL = URL(string: "https://example.com")!
    let path: Path = []
    let parameters: Parameters = [:]
  }
  
  override func setUp() {
    // Clear mock requests before each test.
    MockURLProtocol.mockRequests = []
  }
  
  func testRequestTimedOut() async throws {
    let exchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com")!),
      response: nil,
      error: .requestTimedOut
    )
    
    let session = URLSession(configuration: sessionConfiguration)
    MockURLProtocol.mockRequests.insert(exchange)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))
    
    do {
      _ = try await networkManager.request(DemoGetRequest())
    } catch {
      guard case .requestTimedOut = error as? NetworkError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail("Was expecting a request timed out.")
  }
  
  func testHostNotFound() async throws {
    let exchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com")!),
      response: nil,
      error: .hostNotFound
    )
    
    let session = URLSession(configuration: sessionConfiguration)
    MockURLProtocol.mockRequests.insert(exchange)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))
    
    do {
      _ = try await networkManager.request(DemoGetRequest())
    } catch {
      guard case .hostNotFound = error as? NetworkError else {
        XCTFail("Was expecting a host not found but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail("Was expecting a request timed out.")
  }
  
  func testNoInternetConnection() async throws {
    let exchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com")!),
      response: nil,
      error: .notConnectedToInternet
    )
    
    let session = URLSession(configuration: sessionConfiguration)
    MockURLProtocol.mockRequests.insert(exchange)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))
    
    do {
      _ = try await networkManager.request(DemoGetRequest())
    } catch {
      guard case .noInternetConnection = error as? NetworkError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail("Was expecting a request timed out.")
  }
}
