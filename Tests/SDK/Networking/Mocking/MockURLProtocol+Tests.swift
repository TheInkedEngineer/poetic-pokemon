//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class MockURLProtocolTests: XCTestCase {
  
  /// The `URLSession` instance using the `MockURLProtocol`.
  private static var session: URLSession!
  
  private static var networkExchangeOne: MockNetworkExchange!
  
  private static var networkExchangeOneDuplicate: MockNetworkExchange!
  
  private static var networkExchangeTwo: MockNetworkExchange!
  
  override class func setUp() {
    networkExchangeOne = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: [:]
      )
    )
    
    networkExchangeOneDuplicate = networkExchangeOne
    
    networkExchangeTwo = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://random.com")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: [:]
      )
    )
    
    let sessionConfiguration: URLSessionConfiguration = {
      let configuration = URLSessionConfiguration.default
      configuration.protocolClasses = [MockURLProtocol.self]
      
      return configuration
    }()
    
    session = URLSession(configuration: sessionConfiguration)
  }
  
  override func setUp() {
    MockURLProtocol.mockRequests = []
    MockURLProtocol.shouldCheckQueryParameters = false
  }
  
  func testMockRequestsAreNotBeingDuplicated() {
    MockURLProtocol.mockRequests.insert(Self.networkExchangeOne)
    MockURLProtocol.mockRequests.insert(Self.networkExchangeTwo)
    MockURLProtocol.mockRequests.insert(Self.networkExchangeOneDuplicate)
    
    XCTAssertEqual(MockURLProtocol.mockRequests.count, 2)
  }
  
  func testRequestReturnsCorrectData() {
    MockURLProtocol.mockRequests.insert(Self.networkExchangeOne)
    
    let expectation = XCTestExpectation(description: "Data is correctly recovered")
    
    let dataTask = Self.session.dataTask(
      with: Self.networkExchangeOne.urlRequest.url!
    ) { data, _, _ in
      
      XCTAssertNotNil(data)
      XCTAssertEqual(data, Self.networkExchangeOne.response!.data)
      
      expectation.fulfill()
    }
    
    dataTask.resume()
    
    wait(for: [expectation], timeout: 3)
  }
  
  func testUnmappedRequestReturnsError() {
    let expectation = XCTestExpectation(description: "Request throws route not found error.")
    
    let dataTask = Self.session.dataTask(
      with: Self.networkExchangeOne.urlRequest.url!
    ) { _, _, error in
      
      let mockError = MockError(rawValue: (error! as NSError).code)
      XCTAssertEqual(mockError?.urlError.code, .resourceUnavailable)
      
      expectation.fulfill()
    }
    
    dataTask.resume()
    
    wait(for: [expectation], timeout: 1)
  }
  
  func testShouldCheckForQueryIsTrueChecksForQuery() {
    let expectation = XCTestExpectation(description: "Request is found.")
    
    let networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://random.com?name=firas")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.shouldCheckQueryParameters = true
    MockURLProtocol.mockRequests.insert(networkExchange)
    
    let dataTask = Self.session.dataTask(
      with: URL(string: "https://random.com?name=firas")!
    ) { _, _, error in
      
      XCTAssertNil(error)
      
      expectation.fulfill()
    }
    
    dataTask.resume()
    
    wait(for: [expectation], timeout: 1)
  }
  
  func testShouldCheckForQueryIsTrueChecksForQueryAndDoesntFindRequest() {
    let expectation = XCTestExpectation(description: "Request is found.")
    
    let networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://random.com?name=firas")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.shouldCheckQueryParameters = true
    MockURLProtocol.mockRequests.insert(networkExchange)
    
    let dataTask = Self.session.dataTask(
      with: URL(string: "https://random.com")!
    ) { _, _, error in
      
      let mockError = MockError(rawValue: (error! as NSError).code)
      XCTAssertEqual(mockError?.urlError.code, .resourceUnavailable)
      
      expectation.fulfill()
    }
    
    dataTask.resume()
    
    wait(for: [expectation], timeout: 1)
  }
}
