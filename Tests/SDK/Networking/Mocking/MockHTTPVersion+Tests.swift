//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class MockHTTPVersionTests: XCTestCase {
  func testRawValuesAreCorrect() {
    XCTAssertEqual(MockHTTPVersion.onePointOne.rawValue, "HTTP/1.1")
  }
}
