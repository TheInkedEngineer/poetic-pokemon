//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class TestMockNetworkExchangeTests: XCTestCase {
  
  var networkExchange: MockNetworkExchange!
  
  override func setUp() {
    var request = URLRequest(url: URL(string: "https://example.com")!)
    request.addValue("value2", forHTTPHeaderField: "key2")
    
    networkExchange = MockNetworkExchange(
      urlRequest: request,
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: ["key": "value"]
      )
    )
  }
  
  func testObjectCreatedCorrectly() {
    XCTAssertEqual(networkExchange.urlRequest.url?.absoluteString, "https://example.com")
    XCTAssertEqual(networkExchange.response!.statusCode, .code200)
    XCTAssertEqual(networkExchange.response!.httpVersion, .onePointOne)
    XCTAssertEqual(networkExchange.response!.headers.count, 1)
    XCTAssertTrue(networkExchange.response!.headers.keys.contains("key"))
    XCTAssertTrue(networkExchange.response!.headers.values.contains("value"))
    XCTAssertEqual(String(data: networkExchange.response!.data ?? Data(), encoding: .utf8), "Some data")
  }
  
  func testProperHeaderMerging() {
    var request = URLRequest(url: URL(string: "https://example.com")!)
    request.addValue("value2", forHTTPHeaderField: "key")
    
    networkExchange = MockNetworkExchange(
      urlRequest: request,
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "Some data".data(using: .utf8),
        headers: ["key": "value"]
      )
    )
    
    XCTAssertEqual(networkExchange.urlResponse!.value(forHTTPHeaderField: "key"), "value")
  }
  
  func testNoResponseReturnsNilURLResponse() {
    var request = URLRequest(url: URL(string: "https://example.com")!)
    request.addValue("value2", forHTTPHeaderField: "key")
    
    networkExchange = MockNetworkExchange(urlRequest: request, response: nil)
    
    XCTAssertEqual(networkExchange.urlResponse, nil)
  }
  
  func testURLResponseCreatedCorrectly() {
    let expectedResponse = HTTPURLResponse(
      url: URL(string: "https://example.com")!,
      statusCode: 200,
      httpVersion: "HTTP/1.1",
      headerFields: ["key": "value", "key2": "value2"]
    )
    
    XCTAssertEqual(networkExchange.urlResponse!.url, expectedResponse?.url)
    XCTAssertEqual(networkExchange.urlResponse!.statusCode, expectedResponse?.statusCode)
    XCTAssertEqual(networkExchange.urlResponse!.allHeaderFields.count, 2)
    XCTAssertEqual(
      networkExchange.urlResponse!.allHeaderFields["key"] as? String,
      expectedResponse?.allHeaderFields["key"] as? String
    )
    XCTAssertEqual(
      networkExchange.urlResponse!.allHeaderFields["key2"] as? String,
      expectedResponse?.allHeaderFields["key2"] as? String
    )
  }
}
