//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class MockStatusCodeTests: XCTestCase {
  func testRawValuesAreCorrect() {
    XCTAssertEqual(MockStatusCode.code200.rawValue, 200)
    XCTAssertEqual(MockStatusCode.code429.rawValue, 429)
    XCTAssertEqual(MockStatusCode.code404.rawValue, 404)
    XCTAssertEqual(MockStatusCode.code500.rawValue, 500)
  }
}
