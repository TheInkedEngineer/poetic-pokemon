//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class MockableTests: XCTestCase {
  func testMockableRequestWithNoFile() {
    struct FakeRequest: Mockable {}
    enum File: String {
      case fake
    }
    
    let exchange = FakeRequest.mockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://google.com")!),
      mockDataFile: File.fake
    )
    
    XCTAssertEqual(exchange.response!.statusCode, .code404)
    XCTAssertEqual(exchange.response!.data, nil)
  }
}
