//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class MockResponseTests: XCTestCase {
  func testSimpleObjectCorrectlyInitialized() {
    let response = MockResponse(
      statusCode: .code200,
      httpVersion: .onePointOne,
      data: nil,
      headers: [:]
    )
    
    XCTAssertEqual(response.statusCode, .code200)
    XCTAssertEqual(response.httpVersion, .onePointOne)
    XCTAssertNil(response.data)
    XCTAssertEqual(response.headers.count, 0)
  }
  
  func testFullObjectCorrectlyInitialized() {
    let response = MockResponse(
      statusCode: .code200,
      httpVersion: .onePointOne,
      data: "Some data".data(using: .utf8),
      headers: ["key1": "value1", "key2": "value2"]
    )
    
    XCTAssertEqual(response.statusCode, .code200)
    XCTAssertEqual(response.httpVersion, .onePointOne)
    XCTAssertEqual(String(data: response.data ?? Data(), encoding: .utf8), "Some data")
    XCTAssertEqual(response.headers.count, 2)
    XCTAssertTrue(response.headers.keys.contains("key1"))
    XCTAssertTrue(response.headers.keys.contains("key2"))
    XCTAssertTrue(response.headers.values.contains("value1"))
    XCTAssertTrue(response.headers.values.contains("value2"))
  }
}
