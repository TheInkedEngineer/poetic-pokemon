//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class DecodableRequestTests: XCTestCase {
  struct DemoGetRequest: DecodableRequest {
    typealias ResponseModel = Empty
    
    let method: HTTPMethod = .get
    let baseURL: URL = URL(string: "https://example.com")!
    let path: Path = ["path", "to", "resource"]
  }
  
  struct DemoPostRequest: DecodableRequest {
    typealias ResponseModel = Empty
    
    let method: HTTPMethod = .post
    let baseURL: URL = URL(string: "https://example.com")!
    let path: Path = ["path", "to", "resource"]
    let parameters: Parameters = ["key": "value"]
  }
  
  func testObjectAndDefaultValuesProperlyGenerated() {
    let demoGetRequest = DemoGetRequest()
    let demoPostRequest = DemoPostRequest()
    
    XCTAssertEqual(demoGetRequest.method, .get)
    XCTAssertEqual(demoGetRequest.baseURL, URL(string: "https://example.com"))
    XCTAssertEqual(demoGetRequest.path, ["path", "to", "resource"])
    XCTAssertEqual(demoGetRequest.encoding, .queryString)
    XCTAssertEqual(demoGetRequest.timeout, 20)
    XCTAssertEqual(demoGetRequest.parameters, [:])
    
    XCTAssertEqual(demoPostRequest.method, .post)
    XCTAssertEqual(demoPostRequest.encoding, .httpBody)
    XCTAssertEqual(demoPostRequest.parameters, ["key": "value"])
  }
  
  func testDataCorrectlyEncodedInBody() {
    let demoRequest = DemoPostRequest()
    let expectedBody = "{\"key\":\"value\"}".data(using: .utf8)
    
    let encodedRequest = demoRequest.encoding.encode(demoRequest.asURLRequest(), with: demoRequest.parameters, using: demoRequest.dataEncoder)
    
    XCTAssertEqual(expectedBody, encodedRequest.httpBody)
    XCTAssertEqual(encodedRequest.value(forHTTPHeaderField: "Content-Type"), "application/json")
  }
  
  func testQueryStringDoesNotChangeAnything() {
    let demoRequest = DemoGetRequest()
    let encodedRequest = demoRequest.encoding.encode(demoRequest.asURLRequest(), with: demoRequest.parameters, using: demoRequest.dataEncoder)
    
    XCTAssertNil(encodedRequest.httpBody)
    XCTAssertEqual(demoRequest.asURLRequest(), encodedRequest)
  }
  
  func testURLRequestProperlyGenerated() {
    let demoRequest = DemoPostRequest()
    let request = demoRequest.asURLRequest()
    
    XCTAssertEqual(request.httpMethod, "POST")
    XCTAssertEqual(request.httpBody, "{\"key\":\"value\"}".data(using: .utf8))
    XCTAssertEqual(request.url?.absoluteString, "https://example.com/path/to/resource")
    XCTAssertEqual(request.timeoutInterval, demoRequest.timeout)
  }
}
