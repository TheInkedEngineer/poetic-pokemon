//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class TranslationRequestsTests: XCTestCase {
  func testPathGetsGeneratedCorrectly() {
    let request = Requests.Translate.Shakespeare(text: "This is some text")
    
    XCTAssertEqual(request.baseURL, BaseURL.translationAPI)
    XCTAssertEqual(request.path, ["shakespeare"])
    XCTAssertEqual(request.method, .post)
    XCTAssertEqual(request.parameters, ["text": "This is some text"])
  }
}
