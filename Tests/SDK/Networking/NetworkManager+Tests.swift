//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class NetworkManagerTests: XCTestCase {
  
  private var networkExchange: MockNetworkExchange!
  
  private var sessionConfiguration: URLSessionConfiguration = {
    let configuration = URLSessionConfiguration.default
    configuration.protocolClasses = [MockURLProtocol.self]
    
    return configuration
  }()
  
  override func setUp() {
    // Clear mock requests before each test.
    MockURLProtocol.mockRequests = []
  }
    
  private struct DemoGetRequest: DecodableRequest {
    typealias ResponseModel = DemoResponse
    
    let method: HTTPMethod = .get
    let baseURL: URL = URL(string: "https://example.com")!
    let path: Path = ["path", "to", "resource"]
    let parameters: Parameters = [:]
  }
  
  private struct DemoResponse: Decodable, Equatable {
    let text: String
  }
  
  private struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
    let session: URLSession
  }
  
  func testRequestCorrectlyParsesResponse() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "{\"text\": \"Some data\"}".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    
    let expectedResponse = DemoResponse(text: "Some data")
    let request = DemoGetRequest()
    
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))
  
    let actualResponse = try await networkManager.request(request)
    XCTAssertEqual(actualResponse, expectedResponse)
  }
  
  func testRequestCorrectlyCatchesParsingError() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code200,
        httpVersion: .onePointOne,
        data: "{\"text2\": \"Some data\"}".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))
  
    do {
      _ = try await networkManager.request(DemoGetRequest())
      XCTFail("Was expecting to fail, but passed instead.")
    } catch {
      guard case .decodingError = error as? NetworkError else {
        XCTFail("Was expecting a server error but got \(error) instead")
        return
      }
    }
  }
  
  func testRequestCatches500Error() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code500,
        httpVersion: .onePointOne,
        data: "Internal Server Error 500".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))

    do {
      _ = try await networkManager.request(DemoGetRequest())
      XCTFail("Was expecting to fail, but passed instead.")
    } catch {
      guard case .serverError = error as? NetworkError else {
        XCTFail("Was expecting a server error but got \(error) instead")
        return
      }
    }
  }
  
  func testRequestCatches403Error() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code403,
        httpVersion: .onePointOne,
        data: "Not Found".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))

    do {
      _ = try await networkManager.request(DemoGetRequest())
      XCTFail("Was expecting to fail, but passed instead.")
    } catch {
      guard case let .clientError(code) = error as? NetworkError, code == 403 else {
        XCTFail("Was expecting a server error but got \(error) instead")
        return
      }
    }
  }
  
  func testRequestCatches404Error() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code404,
        httpVersion: .onePointOne,
        data: "Not Found".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))

    do {
      _ = try await networkManager.request(DemoGetRequest())
      XCTFail("Was expecting to fail, but passed instead.")
    } catch {
      guard case .resourceNotFound = error as? NetworkError else {
        XCTFail("Was expecting a server error but got \(error) instead")
        return
      }
    }
  }
  
  func testRequestCatches429Error() async throws {
    networkExchange = MockNetworkExchange(
      urlRequest: URLRequest(url: URL(string: "https://example.com/path/to/resource")!),
      response: MockResponse(
        statusCode: .code429,
        httpVersion: .onePointOne,
        data: "Too many requests".data(using: .utf8),
        headers: [:]
      )
    )
    
    MockURLProtocol.mockRequests.insert(networkExchange)
    let session = URLSession(configuration: sessionConfiguration)
    let networkManager = NetworkManager(with: NetworkManagerConfiguration(session: session))

    do {
      _ = try await networkManager.request(DemoGetRequest())
      XCTFail("Was expecting to fail, but passed instead.")
    } catch {
      guard case .tooManyRequests = error as? NetworkError else {
        XCTFail("Was expecting a server error but got \(error) instead")
        return
      }
    }
  }
}
