//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class PokemonRequestsTests: XCTestCase {
  func testInformationPathGetsGeneratedCorrectly() {
    let request = Requests.Pokemon.Information("Charizard")
    
    XCTAssertEqual(request.baseURL, BaseURL.pokemonAPI)
    XCTAssertEqual(request.path, ["pokemon-species", "charizard"])
    XCTAssertEqual(request.method, .get)
    XCTAssertEqual(request.parameters, [:])
  }
  
  func testSpritePathGetsGeneratedCorrectly() {
    let request = Requests.Pokemon.Sprite("Charizard")
    
    XCTAssertEqual(request.baseURL, BaseURL.pokemonAPI)
    XCTAssertEqual(request.path, ["pokemon", "charizard"])
    XCTAssertEqual(request.method, .get)
    XCTAssertEqual(request.parameters, [:])
  }
}
