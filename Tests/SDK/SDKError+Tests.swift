//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class TestSDKErrors: XCTestCase {
  func testCorrectCopy() {
    XCTAssertEqual(PoeticPokemon.SDKError.resourceNotFound.message, TextContent.Error.resourceNotFound)
    XCTAssertEqual(PoeticPokemon.SDKError.connectionIssues.message, TextContent.Error.connectionIssues)
    XCTAssertEqual(PoeticPokemon.SDKError.tooManyRequests.message, TextContent.Error.tooManyRequests)
    XCTAssertEqual(PoeticPokemon.SDKError.providerError.message, TextContent.Error.providerError)
    XCTAssertEqual(PoeticPokemon.SDKError.fatal.message, TextContent.Error.fatal)
  }
}
