//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class ServicesTest: XCTestCase {
  struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
    var session: URLSession
  }
  
  var session: URLSession {
    let configuration: URLSessionConfiguration = {
      let configuration = URLSessionConfiguration.default
      configuration.protocolClasses = [MockURLProtocol.self]
      
      return configuration
    }()
    
    let session = URLSession(configuration: configuration)
    MockURLProtocol.mockRequests = [
      Requests.Pokemon.Information.mockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("Charizard").asURLRequest(),
        mockDataFile: MockDataFile.pokemonSpecies
      ),
      
      Requests.Pokemon.Sprite.mockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("Charizard").asURLRequest(),
        mockDataFile: MockDataFile.pokemon
      ),
      
      Requests.Translate.Shakespeare.mockNetworkExchange(
        urlRequest: Requests.Translate.Shakespeare(text: "Some description").asURLRequest(),
        mockDataFile: MockDataFile.shakespearean
      )
    ]
    
    // Error cases
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("charizard1").asURLRequest(),
        response: nil,
        error: .notConnectedToInternet
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("charizard2").asURLRequest(),
        response: nil,
        error: .requestTimedOut
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("charizard3").asURLRequest(),
        response: MockResponse(statusCode: .code404)
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("charizard4").asURLRequest(),
        response: MockResponse(statusCode: .code429)
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Sprite("charizard5").asURLRequest(),
        response: MockResponse(statusCode: .code500)
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("charizard1").asURLRequest(),
        response: nil,
        error: .notConnectedToInternet
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("charizard2").asURLRequest(),
        response: nil,
        error: .requestTimedOut
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("charizard3").asURLRequest(),
        response: MockResponse(statusCode: .code404)
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("charizard4").asURLRequest(),
        response: MockResponse(statusCode: .code429)
      )
    )
    
    MockURLProtocol.mockRequests.insert(
      MockNetworkExchange(
        urlRequest: Requests.Pokemon.Information("charizard5").asURLRequest(),
        response: MockResponse(statusCode: .code500)
      )
    )
    
    return session
  }
  
  var service: PoeticPokemon.Services {
    PoeticPokemon.Services(using: NetworkManagerConfiguration(session: session))
  }
  
  func testGetPokemonInformation() async throws {
    let expectedOutput = """
    Charizard flies 'round the sky in search of powerful opponents. 't breathes fire of such most wondrous heat yond 't melts aught. However,  't nev'r turns its fiery breath on any opponent weaker than itself.
    """
    
    let actualOutput = try await service.getShakespeareanDescription(for: "Charizard")
    
    XCTAssertEqual(expectedOutput, actualOutput)
  }
  
  func testGetPokemonSprite() async throws {
    let expectedOutput = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png")!
    
    let actualOutput = try await service.getSprite(for: "Charizard")
    
    XCTAssertEqual(expectedOutput, actualOutput)
  }
  
  func testSpriteServicesErrorNoConnection() async throws {
    do {
      _ = try await service.getSprite(for: "Charizard1")
    } catch {
      guard case .connectionIssues = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testSpriteServicesErrorTimedOut() async throws {
    do {
      _ = try await service.getSprite(for: "Charizard2")
    } catch {
      guard case .connectionIssues = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testSpriteServicesErrorNotFound() async throws {
    do {
      _ = try await service.getSprite(for: "Charizard3")
    } catch {
      guard case .resourceNotFound = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testSpriteServicesErrorTooManyRequests() async throws {
    do {
      _ = try await service.getSprite(for: "Charizard4")
    } catch {
      guard case .tooManyRequests = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testSpriteServicesErrorServer500() async throws {
    do {
      _ = try await service.getSprite(for: "Charizard5")
    } catch {
      guard case .providerError = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testInformationServicesErrorNoConnection() async throws {
    do {
      _ = try await service.getShakespeareanDescription(for: "Charizard1")
    } catch {
      guard case .connectionIssues = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testInformationServicesErrorTimedOut() async throws {
    do {
      _ = try await service.getShakespeareanDescription(for: "Charizard2")
    } catch {
      guard case .connectionIssues = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testInformationServicesErrorNotFound() async throws {
    do {
      _ = try await service.getShakespeareanDescription(for: "Charizard3")
    } catch {
      guard case .resourceNotFound = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testInformationServicesErrorTooManyRequests() async throws {
    do {
      _ = try await service.getShakespeareanDescription(for: "Charizard4")
    } catch {
      guard case .tooManyRequests = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }
  
  func testInformationServicesErrorServer500() async throws {
    do {
      _ = try await service.getShakespeareanDescription(for: "Charizard5")
    } catch {
      guard case .providerError = error as? PoeticPokemon.SDKError else {
        XCTFail("Was expecting a request timed out but got \(error) instead.")
        return
      }
      
      return
    }
    
    XCTFail()
  }

  func testNotNetworkErrorReturnsFatalError() {
    let error = service.managerError(NSError())
    
    XCTAssertEqual(error, .fatal)
  }
}
