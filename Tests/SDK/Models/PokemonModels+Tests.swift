//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class PokemonModelsTests: XCTestCase {
  let informationMockJSON = """
  {
    "flavor_text_entries": [
      { "flavor_text": "This is the first description", "language": { "name": "en" } },
      { "flavor_text": "This is the second description", "language": { "name": "en" } },
      { "flavor_text": "This is the third description", "language": { "name": "en" } },
      { "flavor_text": "Questa in italiano non conta", "language": { "name": "it" } }
    ]
  }
  """.data(using: .utf8)!
  
  let spritesMockJSON = """
  {
    "sprites": {
      "back_default": "https://raw.githubuserconten.com/PokeAPI/sprites/master/sprites/pokemon/back/6.png",
      "back_female": null,
      "back_shiny": "https://raw.githubuserconten.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/6.png",
      "back_shiny_female": null,
      "front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/.png",
      "front_female": null,
      "front_shiny": "https://raw.githubuserconten.com/PokeAPI/sprites/master/sprites/pokemon/shiny/6.png",
      "front_shiny_female": null,
      "other": {
        "dream_world": {
          "front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/6.svg",
          "front_female": null
        },
        "official-artwork": {
          "front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/6.png"
        }
      }
    }
  }
  """.data(using: .utf8)!
  
  let expectedDecodedData = [
    "Questa in italiano non conta",
    "This is the first description",
    "This is the second description",
    "This is the third description"
  ]
  
  // MARK: Information
  
  func testInformationResponseModelDecodesCorrectly() {
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Pokemon.Information.self,
      from: informationMockJSON
    )
    
    XCTAssertEqual(decodedData?.descriptions.map { $0.text }.sorted(by: <), expectedDecodedData)
  }
  
  func testInformationResponseModelNormalizesCorrectly() {
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Pokemon.Information.self,
      from: informationMockJSON
    )
    
    XCTAssertTrue(expectedDecodedData.contains(decodedData!.normalize().description))
  }
  
  func testInformationResponseModelNormalizesMissingDescriptionCorrectly() {
    let mockJSON = """
    {
      "flavor_text_entries": []
    }
    """.data(using: .utf8)!
    
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Pokemon.Information.self,
      from: mockJSON
    )
    
    XCTAssertEqual(decodedData!.normalize().description, TextContent.Error.noDescriptionFound)
  }
  
  // MARK: Sprite
  
  func testSpriteResponseModelDecodesCorrectly() {
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Pokemon.Sprite.self,
      from: spritesMockJSON
    )
    
    let expectedURL = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/.png")
    
    XCTAssertEqual(decodedData?.url, expectedURL)
  }
  
  func testSpriteResponseModelNormalizesCorrectly() {
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Pokemon.Sprite.self,
      from: spritesMockJSON
    )
    
    let expectedURL = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/.png")
    
    XCTAssertEqual(decodedData?.normalize().url, expectedURL)
  }
}
