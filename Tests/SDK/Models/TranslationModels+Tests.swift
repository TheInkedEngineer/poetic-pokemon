//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import XCTest

@testable import PoeticPokemonSDK

class TranslationModelsTests: XCTestCase {
  let mockJSON = """
  {
    "success": {
      "total": 1
    },
    "contents": {
      "translated": "Charizard flies 'round the sky in search of powerful opponents. 't breathes fire of such most wondrous heat yond 't melts aught. However,  't nev'r turns its fiery breath on any opponent weaker than itself.",
      "text": "Charizard flies around the sky in search of powerful opponents.\\nIt breathes fire of such great heat that it melts anything.\\nHowever, it never turns its fiery breath on any opponent\\nweaker than itself.",
      "translation": "shakespeare"
    }
  }
  """
    .data(using: .utf8)!
  
  let expectedDecodedData = "Charizard flies 'round the sky in search of powerful opponents. 't breathes fire of such most wondrous heat yond 't melts aught. However,  't nev'r turns its fiery breath on any opponent weaker than itself."
  
  func testResponseModelDecodesCorrectly() {
    let decodedData = try! JSONDecoder().decode(
      Models.Networking.Response.Translation.Shakespeare.self,
      from: mockJSON
    )
    
    XCTAssertEqual(decodedData.content.translated, expectedDecodedData)
  }
  
  func testResponseModelNormalizesCorrectly() {
    let decodedData = try? JSONDecoder().decode(
      Models.Networking.Response.Translation.Shakespeare.self,
      from: mockJSON
    )
    
    XCTAssertEqual(decodedData?.normalize().text, expectedDecodedData.replacingOccurrences(of: "\\n", with: " "))
  }
}
