//
//  Poetic Pokemon
//  Firas Safa
//

import Foundation
import SwiftUI
import ViewInspector
import XCTest

@testable import PoeticPokemonSDK

class UserInterfaceTests: XCTestCase {
  private struct PoeticPokemonConfiguration: PoeticPokemonConfigurationProvider {
    var networkManagerConfiguration: NetworkManagerConfigurationProvider = NetworkManagerConfiguration()
    var visualConfiguration: VisualSettingsConfigurationProvider = VisualSettingsConfiguration()
  }

  private struct NetworkManagerConfiguration: NetworkManagerConfigurationProvider {
    var session: URLSession = URLSession(configuration: .ephemeral)
  }

  private struct VisualSettingsConfiguration: VisualSettingsConfigurationProvider {
    var brandColor: Color = .red
    var textColor: Color = .white
    var font: Font = .subheadline
  }
  
  override class func setUp() {
    let _ = PoeticPokemon(using: PoeticPokemonConfiguration())
  }
  
  func testPoemCardUI() throws {
    let description = "Its transformation ability is perfect. However, if made to laugh, it can't maintain its disguise."
    let view = PoeticPokemon.UI.PoemCard(text: description)

    let vStack = try view.inspect().vStack()
    let textView = try vStack.text(0)
    
    XCTAssertEqual(view.text, description)
    XCTAssertEqual(try textView.string(), description)
    XCTAssertEqual(try textView.attributes().font(), .subheadline)
    XCTAssertEqual(try textView.attributes().foregroundColor(), .white)
    XCTAssertTrue(textView.hasPadding(.all))
  }
  
  func testPokeSpriteUI() throws {
    let imageURL = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/132.png")!
    let view = PoeticPokemon.UI.PokeSprite(url: imageURL)

    let vStack = try view.inspect().vStack()
    
    XCTAssertEqual(view.url, imageURL)
    XCTAssertTrue(vStack.hasPadding(.all))
    XCTAssertEqual(try vStack.cornerRadius(), 75)
  }
}

extension PoeticPokemon.UI.PoemCard: Inspectable {}
extension PoeticPokemon.UI.PokeSprite: Inspectable {}
extension AsyncImage: Inspectable {}
